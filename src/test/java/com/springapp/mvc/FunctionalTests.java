package com.springapp.mvc;

import com.springapp.mvc.services.Project;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.Response;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

public class FunctionalTests {
    public static void main(String[] args) {
        /** These tests must run in this order and the database must be clean before running */
        testPostProject();
        testPutProject();
        testPostMeeting();
        testGetProject();
        testDeleteProject();
    }

    static void testPostProject() {
        try {
            System.out.println("*** Create a new project ***");

            Client client = ClientBuilder.newClient();

            String xml = "<project>\n" +
                    "  <name>cs429</name>\n" +
                    "  <description>binary bomb</description>\n" +
                    "</project>";

            Response response = client.target("http://localhost:8080/myeavesdrop/projects/").request().post(Entity.xml(xml));

            if(response.getStatus() != 201 && response.getStatus() != 204) {
                System.out.println(" -> Failed to make POST project call");
            }
            else {
                System.out.println(" -> Successfully created project call");
            }

            /** Verify the project was created correctly */
            Class.forName("com.mysql.jdbc.Driver");
            Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/openstack_projects", "root", "");

            String sql = "select * from projects where name=" + "\"" + "cs429" + "\"";
            PreparedStatement preparedStatement = conn.prepareStatement(sql);
            ResultSet resultSet = preparedStatement.executeQuery();

            Project project = new Project();
            if(resultSet.next()) {
                project.setName(resultSet.getString("name"));
                project.setDescription(resultSet.getString("description"));
            }

            if(project.getName().equalsIgnoreCase("cs429") && project.getDescription().equalsIgnoreCase("binary bomb")) {
                System.out.println(" -> Create project verified in the database");
            } else {
                System.out.println(" -> Failed to verify database record for new project");
            }

            response.close();
        } catch (Exception e) {
            System.out.println(" -> Error occurred. Failed to POST project");
        }

    }

    static void testPutProject() {
        try {
            System.out.println("\n*** Update project ***");

            Client client = ClientBuilder.newClient();

            String xml = "<project><description>NOT binary bomb</description></project>";

            Response response = client.target("http://localhost:8080/myeavesdrop/projects/cs429").request().put(Entity.xml(xml));

            if (response.getStatus() != 201 && response.getStatus() != 204) {
                System.out.println(" -> Failed to make PUT project call");
            } else {
                System.out.println(" -> Successfully made PUT project call");
            }

            /** Verify that the description was updated in the database */
            Class.forName("com.mysql.jdbc.Driver");
            Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/openstack_projects", "root", "");

            String sql = "select * from projects where name=" + "\"" + "cs429" + "\"";
            PreparedStatement preparedStatement = conn.prepareStatement(sql);
            ResultSet resultSet = preparedStatement.executeQuery();

            Project project = new Project();
            if (resultSet.next()) {
                project.setName(resultSet.getString("name"));
                project.setDescription(resultSet.getString("description"));
            }

            if (project.getName().equalsIgnoreCase("cs429") && project.getDescription().equalsIgnoreCase("not binary bomb")) {
                System.out.println(" -> Update to project verified in the database");
            } else {
                System.out.println(" -> Failed to verify database record for updated project");
            }

            response.close();
        } catch(Exception e) {
            System.out.println(" -> Error occurred. Failed to update project");
        }

    }

    private static void testPostMeeting() {
        try {
            System.out.println("\n*** Create a new meeting ***");

            Client client = ClientBuilder.newClient();

            String xml = "<meeting>\n" +
                    "  <name>m1</name>\n" +
                    "  <link>m1_link</link>\n" +
                    "  <year>2014</year>\n" +
                    "</meeting>\n";

            Response response = client.target("http://localhost:8080/myeavesdrop/projects/cs429/meetings").request().post(Entity.xml(xml));

            if (response.getStatus() != 201 && response.getStatus() != 204) {
                System.out.println(" -> Failed to make POST meeting call");
            } else {
                System.out.println(" -> Successfully made POST meeting call");
            }

            // TODO verify the updated project in the database... haven't because it was throwing strange errors

            response.close();
        } catch(Exception e) {
            System.out.println(" -> Error occurred. Failed to add meeting");
        }
    }

    private static void testGetProject() {
        /** Assumes that testPostProject and testPostMeeting have successfuly run */
        Client client = ClientBuilder.newClient();

        System.out.println("\n*** Get project and its associated meetings ***");

        String response = client.target("http://localhost:8080/myeavesdrop/projects/cs429").request().get(String.class);
        String expectedResponse = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\"?>\n" +
                "<project>\n" +
                "    <name>cs429</name>\n" +
                "    <description>not binary bomb</description>\n" +
                "    <meetings>\n" +
                "        <meeting>\n" +
                "            <name>m1</name>\n" +
                "            <link>m1_link</link>\n" +
                "            <year>2014</year>\n" +
                "        </meeting>\n" +
                "    </meetings>\n" +
                "</project>";

        if(!response.replaceAll("\\s+","").equalsIgnoreCase(expectedResponse.replaceAll("\\s+", ""))) {
            System.out.println(" -> Failed to get project and its associated meetings");
        }
        else {
            System.out.println(" -> Successful GET project and its associated meetings");
        }
    }

    private static void testDeleteProject() {
        try {
            System.out.println("\n*** Delete a project ***");

            Client client = ClientBuilder.newClient();

            Response response = client.target("http://localhost:8080/myeavesdrop/projects/cs429").request().delete();

            if (response.getStatus() != 201 && response.getStatus() != 204) {
                System.out.println(" -> Failed to make DELETE project call");
            } else {
                System.out.println(" -> Successfully made DELETE project call");
            }

            /** Verify record was deleted in the database */
            Class.forName("com.mysql.jdbc.Driver");
            Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/openstack_projects", "root", "");

            String sql = "select * from projects where name=" + "\"" + "cs429" + "\"";
            PreparedStatement preparedStatement = conn.prepareStatement(sql);
            ResultSet resultSet = preparedStatement.executeQuery();

            if(!resultSet.isFirst()) {
                System.out.println(" -> Delete project verified in the database");
            } else {
                System.out.println(" -> Delete project not verified in the database");
            }

            response.close();
        } catch(Exception e) {
            System.out.println(" -> Failed to delete project");
        }
    }

}
