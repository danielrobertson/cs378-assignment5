package com.springapp.mvc.services;

import org.w3c.dom.*;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

import java.io.*;
import javax.xml.transform.*;
import javax.xml.transform.dom.*;
import javax.xml.transform.stream.*;

/** @author Daniel Robertson */

public class RestService {
    private DBService dbService;

    public RestService() {
        dbService = new DBService();
    }

    /** Create project and save to database */
    public void postProject(String body) {
        /* Parse XML to populate name and description */
        NodeList nodes = getNodeList(body);
        Project project = new Project();

        for(int i = 0; i < nodes.getLength(); ++i) {
            Node child = nodes.item(i);

            if(child instanceof Element) {
                String value = child.getLastChild().getTextContent().trim();
                String s = child.getNodeName();

                if (s.equalsIgnoreCase("name")) {
                    project.setName(value.toLowerCase());
                } else if (s.equalsIgnoreCase("description")) {
                    project.setDescription(value.toLowerCase());
                }
            }
        }

        /* Save to database */
        dbService.saveProject(project);
    }

    /** Creates and returns a DOMParser Document */
    Document getDocument(String body) {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        factory.setValidating(true);
        factory.setIgnoringElementContentWhitespace(true);
        Document document = null;
        try {
            DocumentBuilder builder = factory.newDocumentBuilder();
            InputStream stream = new ByteArrayInputStream(body.getBytes());
            document = builder.parse(stream);

        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return document;
    }

    /** Updates the description of a project */
    public void putProject(String projectName, String body) {
        /* Parse XML to populate name and description */
        NodeList nodes = getNodeList(body);

        Project project = new Project();
        project.setName(projectName);

        for(int i = 0; i < nodes.getLength(); ++i) {
            Node child = nodes.item(i);

            if(child instanceof Element) {
                String value = child.getLastChild().getTextContent().trim();
                String s = child.getNodeName();

                if(s.equalsIgnoreCase("description")) {
                    project.setDescription(value.toLowerCase());
                }
            }
        }

        /* Save to database */
        dbService.updateProject(project);

    }

    /** Create a meeting for the project */
    public void postMeeting(String project, String body) {
        Meeting meeting = new Meeting();
        meeting.setFk_projects_name(project);

        NodeList nodes = getNodeList(body);

        for(int i = 0; i < nodes.getLength(); ++i) {
            Node child = nodes.item(i);

            if(child instanceof Element) {
                String value = child.getLastChild().getTextContent().trim();
                String s = child.getNodeName();

                if(s.equalsIgnoreCase("name")) {
                    meeting.setName(value.toLowerCase());
                } else if(s.equalsIgnoreCase("link")) {
                    // what is this and how to deal with it?
                    meeting.setLink(value);
                } else if(s.equalsIgnoreCase("year")) {
                    meeting.setYear(Integer.parseInt(value));
                }
            }
        }

        /* Save to database */
        dbService.saveMeeting(meeting);

    }

    /** Gets and returns a Project and its associated meetings */
    public String getProject(String projectName) {
        Object[] pair = dbService.getProjectByName(projectName);
        Project project = (Project) pair[0];
        ArrayList<Meeting> meetings = (ArrayList<Meeting>) pair[1];
        String result = "";

        // build XML for project and its associated meetings
        try {
            DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder docBuilder = docFactory.newDocumentBuilder();

            // project tag
            Document doc = docBuilder.newDocument();
            Element rootElement = doc.createElement("project");
            doc.appendChild(rootElement);

            // project name
            Element nameElement = doc.createElement("name");
            nameElement.appendChild(doc.createTextNode(project.getName()));
            rootElement.appendChild(nameElement);

            // project description
            Element descriptionElement = doc.createElement("description");
            descriptionElement.appendChild(doc.createTextNode(project.getDescription()));
            rootElement.appendChild(descriptionElement);

            // meetings tag
            Element meetingsElement = doc.createElement("meetings");
            rootElement.appendChild(meetingsElement);

            for(Meeting m: meetings) {
                // meeting tag
                Element meetingElement = doc.createElement("meeting");
                meetingsElement.appendChild(meetingElement);

                // meeting name
                Element meetingNameElement = doc.createElement("name");
                meetingNameElement.appendChild(doc.createTextNode(m.getName()));
                meetingElement.appendChild(meetingNameElement);

                // meeting link
                Element meetingLinkElement = doc.createElement("link");
                meetingLinkElement.appendChild(doc.createTextNode(m.getLink()));
                meetingElement.appendChild(meetingLinkElement);

                // meeting year
                Element meetingYearElement = doc.createElement("year");
                meetingYearElement.appendChild(doc.createTextNode(Integer.toString(m.getYear())));
                meetingElement.appendChild(meetingYearElement);
            }


            result = getStringFromDocument(doc);
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        }

        return result;
    }

    /** Convert Document to String */
    public String getStringFromDocument(Document doc)
    {
        try
        {
            DOMSource domSource = new DOMSource(doc);
            StringWriter writer = new StringWriter();
            StreamResult result = new StreamResult(writer);
            TransformerFactory tf = TransformerFactory.newInstance();
            Transformer transformer = tf.newTransformer();
            transformer.transform(domSource, result);
            return writer.toString();
        }
        catch(TransformerException ex)
        {
            ex.printStackTrace();
            return null;
        }
    }

    /** Deletes a project and its associated meetings */
    public void deleteProject(String name) {
        Project project = new Project();
        project.setName(name.toLowerCase());

        /* Save to database */
        dbService.deleteProject(project);
    }

    /** First step in parsing the xml body */
    NodeList getNodeList(String body) {
        body = body.replace("\n", "").replace("\r", "");
        Document document = getDocument(body);
        Element root = document.getDocumentElement();
        return root.getChildNodes();
    }

    /** Close the database connction */
    public void close() {
        dbService.close();
    }
}
