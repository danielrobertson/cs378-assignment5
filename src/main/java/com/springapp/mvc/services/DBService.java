package com.springapp.mvc.services;

import java.sql.*;
import java.util.ArrayList;

/** @author Daniel Robertson */

public class DBService {
    private String url;
    private Connection conn;

    public DBService() {
        url = "jdbc:mysql://localhost:3306/openstack_projects";

        try {
            Class.forName("com.mysql.jdbc.Driver");
            conn = DriverManager.getConnection(url, "root", "");
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    /** Saves a Project */
    public void saveProject(Project project) {
        String sql = "insert into projects(name, description) values(\"" + project.getName() + "\", \"" + project.getDescription() + "\");";
        executeUpdate(sql);
    }

    /** Updates the project description */
    public void updateProject(Project project) {
        String sql = "update projects set description = \"" + project.getDescription() + "\" where name = \"" + project.getName() + "\";";
        executeUpdate(sql);
    }

    /** Saves a Meeting for a Project */
    public void saveMeeting(Meeting meeting) {
        String sql = "insert into meetings(name, link, year, fk_projects_name) values(\"" + meeting.getName() + "\", \"" + meeting.getLink() + "\", " + meeting.getYear() + ", \"" + meeting.getFk_projects_name() + "\");";
        executeUpdate(sql);
    }

    /** Gets a Project and its associated Meetings */
    public Object[] getProjectByName(String projectName) {
        Project project = new Project();
        ArrayList<Meeting> meetings = new ArrayList<Meeting>();

        try {
            /* Projects */
            String sql = "select * from projects where name=" + "\"" + projectName + "\"";
            PreparedStatement preparedStatement = conn.prepareStatement(sql);
            ResultSet resultSet = preparedStatement.executeQuery();

            if(resultSet.next()) {
                project.setName(resultSet.getString("name"));
                project.setDescription(resultSet.getString("description"));
            }

            /* Meetings */
            sql = "select * from meetings where fk_projects_name=" + "\"" + projectName + "\"";
            preparedStatement = conn.prepareStatement(sql);
            resultSet = preparedStatement.executeQuery();

            while(resultSet.next()) {
                Meeting meeting = new Meeting();
                meeting.setName(resultSet.getString("name"));
                meeting.setLink(resultSet.getString("link"));
                meeting.setYear(Integer.parseInt(resultSet.getString("year")));
                meetings.add(meeting);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return new Object[] {project, meetings};
    }

    /** Deletes a Project and meetings associated with it */
    public void deleteProject(Project project) {
        String sql = "delete from projects where name=\"" + project.getName() + "\";";
        executeUpdate(sql);
    }

    /** Execute a SQL query that manipulates data */
    void executeUpdate(String sql) {
        PreparedStatement preparedStatement;

        try {
            preparedStatement = conn.prepareStatement(sql);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /** Close the Connection */
    public void close() {
        try {
            conn.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
