package com.springapp.mvc.services;

/** Represents a meeting database record */
public class Meeting {
    private String name;
    private String link;
    private int year;
    private String fk_projects_name;

    public String getFk_projects_name() {
        return fk_projects_name;
    }

    public void setFk_projects_name(String fk_projects_name) {
        this.fk_projects_name = fk_projects_name;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

}
