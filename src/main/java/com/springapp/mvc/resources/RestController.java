package com.springapp.mvc.resources;

/** @author Daniel Robertson */

import com.springapp.mvc.services.RestService;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.ws.rs.Produces;

@Controller
@RequestMapping("/myeavesdrop/projects/")
public class RestController {

    /** Get and return a project */
    @RequestMapping(value = "{project}", method = RequestMethod.GET)
    @ResponseBody
    @Produces("application/xml")
    public String getProject(@PathVariable String project) {
        RestService restService = new RestService();
        return restService.getProject(project);
    }

    /** Create a project */
    @RequestMapping(method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void postProject(@RequestBody String body) {
        RestService restService = new RestService();
        restService.postProject(body);
    }

    /** Update a project */
    @RequestMapping(value = "{project}", method = RequestMethod.PUT)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void putProject(@PathVariable String project, @RequestBody String body) {
        RestService restService = new RestService();
        restService.putProject(project, body);
    }

    /** Create meeting for a project */
    @RequestMapping(value = "{project}/meetings", method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void postMeeting(@PathVariable String project, @RequestBody String body) {
        RestService restService = new RestService();
        restService.postMeeting(project, body);
    }

    /** Delete a project */
    @RequestMapping(value = "{project}", method = RequestMethod.DELETE)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteProject(@PathVariable String project) {
        RestService restService = new RestService();
        restService.deleteProject(project);
    }

}