CREATE TABLE `openstack_projects`.`projects` (
  `name` VARCHAR(255) NOT NULL,
  `description` VARCHAR(255),
  PRIMARY KEY (`name`));

CREATE TABLE `openstack_projects`.`meetings` (
  `name` VARCHAR(255) NOT NULL,
  `link` VARCHAR(255),
  `year` INT,
  `fk_projects_name` VARCHAR(255),
  PRIMARY KEY (`name`),
  INDEX `fk_projects_name_idx` (`fk_projects_name` ASC),
  CONSTRAINT `fk_projects_name`
    FOREIGN KEY (`fk_projects_name`)
    REFERENCES `openstack_projects`.`projects` (`name`)
    ON DELETE CASCADE
    ON UPDATE NO ACTION);
